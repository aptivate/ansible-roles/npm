def test_system_packages_installed(host):
    for package in ['nodejs', 'npm', 'epel-release']:
        package = host.package(package)
        assert package.is_installed


def test_npm_packages_installed_globally(host):
    output = host.check_output('npm list -g --depth 0')
    assert 'less' in output
