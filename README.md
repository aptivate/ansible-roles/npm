[![pipeline status](https://git.coop/aptivate/ansible-roles/npm/badges/master/pipeline.svg)](https://git.coop/aptivate/ansible-roles/npm/commits/master)

# npm

A role to install NPM and packages.

## Supported Scenarios on CentOS 7

  * Install default `nodejs` and `npm` via Yum.

# Requirements

None.

# Role Variables

  * `npm_global_packages`: NPM packages to install globally.
    * Default is `[]`.

# Dependencies

# Example Playbook

```yaml
- hosts: localhost
  roles:
     - role: npm
       npm_global_packages:
        - less
```

# Testing

```bash
$ pipenv install --dev
$ pipenv run molecule test
```

# License

  * https://www.gnu.org/licenses/gpl-3.0.en.html

# Author Information

  * https://aptivate.org/
  * https://git.coop/aptivate
